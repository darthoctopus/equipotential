#! /usr/bin/python3
# coding=utf-8

## EQUIPOTENTIAL (CHINESE) CHESS
## See http://hyad.es/equipotential

import numpy as np

import pyglet
import pyglet.gl as GL
from pyglet.window import mouse, FPSDisplay
from pyglet import clock
from sys import exit
from functools import reduce

import ctypes
# np.seterr(all='raise')

# configuration
from equipotential.config import PLAYERS
# graphical elements
from equipotential.board import vertices, equipotentials, fieldlines
# rules associated with board
from equipotential.board import reset_board
# visual flourishes
from equipotential.board import e_doubled, e_displ, e_offsets
from equipotential.l10n import pieces, playernames, textcolours
from equipotential.rules import owner, threatlist, check, defeat

ACTIVE_PLAYERS = np.ones(PLAYERS)
def reset():
	global occupied
	global currentturn
	global history
	global histlist
	global threatened
	global selected

	currentturn = 0
	threatened = np.array([0 for i in vertices], dtype=int)
	history = []
	histlist = []
	ACTIVE_PLAYERS[...] = 1
	selected = -1

	occupied = reset_board()

reset()


if __name__ == '__main__':

	# Define some colors
	BLACK = (0., 0., 0., 1.)
	GRAY = (.50, .50, .50, .5)
	WHITE = (1., 1., 1., 1.)
	RED = (1., 0., 0., 1.)
	GREEN = (0., 1., 0., 1.)
	BLUE = (0., 0., 1., 1.)
	PIECE_COLOUR = (.85, .85, .85, .8)

	ZOOM_SMOOTH = 5

	# Radii given in PLANE COORDS
	PIECE_RADIUS = 10 / 400
	VERTEX_RADIUS = 3 / 400
	HIGHLIGHT_RADIUS = 12 / 400

	DISPLAY_SCALE = 6400 # As much as I hate this, we need this so that the text on the pieces doesn't get too fuzzy when zooming in
	FONT = 'Adobe Kaiti Std' # Is there a way for Pyglet to draw vector text? The rendering right now sucks when zoomed out.

	viewport = np.array([0.,0.])
	scale = 400/DISPLAY_SCALE
	viewscale = 400/DISPLAY_SCALE
	zoompos = np.array([0,0])

	try:
		config = GL.Config(sample_buffers=1, samples=8, depth_size=0, double_buffer=True)
		window = pyglet.window.Window(resizable=True, vsync=True, config=config)
	except pyglet.window.NoSuchConfigException:
		window = pyglet.window.Window(resizable=True, vsync=True)
	window.set_caption('Equipotential Chess')

	GL.glMatrixMode(GL.GL_MODELVIEW)
	GL.glTranslatef(window.width/2, window.height/2, 0)
	GL.glScalef(viewscale, viewscale, viewscale)

	fps_display = FPSDisplay(window)


	#########################
	## GRAPHICAL PRIMITIVES
	#########################

	def rescale(vertices):
		global viewport
		global viewscale
		return np.array([(p + viewport)* DISPLAY_SCALE for p in vertices])

	def draw_line(vertices, color, closed = False):
		GL.glColor4f(*color)
		v = rescale(vertices)
		pyglet.graphics.draw(len(vertices), GL.GL_LINE_LOOP if closed else GL.GL_LINE_STRIP, ('v2f', v.flatten() ))

	def draw_circle(pos, r, fill, n = 20):
		GL.glColor4f(*fill)
		v = rescale(circle_vertices(pos, r, n))
		pyglet.graphics.draw(len(v), GL.GL_POLYGON, ('v2f', v.flatten() ))

	eq_list = []
	fl_list = []
	ov_list = []
	vx_list = []
	pc_list = []
	hl_list = []
	lb_list = []

	for eq in equipotentials:
		eq_list.append( pyglet.graphics.vertex_list(len(eq), ('v2f', rescale(eq).flatten())) )
	for fl in fieldlines:
		fl_list.append( pyglet.graphics.vertex_list(len(fl), ('v2f', rescale(fl).flatten())) )
	for hl in e_doubled:
		ov_list.append( pyglet.graphics.vertex_list(len(equipotentials[hl]), ('v2f', rescale((equipotentials[hl] - e_displ[e_doubled.index(hl)]) * (1 + e_offsets[e_doubled.index(hl)] / DISPLAY_SCALE) + e_displ[e_doubled.index(hl)]).flatten())) )
	
	# NUMBER OF VERTICES IN A CIRCLE
	N = 20

	def circle_vertices(pos, r, n = N):
		return (np.array([pos + r * np.array([np.cos(2 * np.pi * i / n), np.sin (2 * np.pi * i / n)]) for i in range(n)]))

	for vx in vertices:
		vx_list.append( pyglet.graphics.vertex_list(N, ('v2f', rescale(circle_vertices(vx, VERTEX_RADIUS)).flatten() ) ) )
	for pc in vertices:
		pc_list.append( pyglet.graphics.vertex_list(N, ('v2f', rescale(circle_vertices(pc, PIECE_RADIUS)).flatten() ) ) )
	for hl in vertices:
		hl_list.append( pyglet.graphics.vertex_list(N, ('v2f', rescale(circle_vertices(hl, HIGHLIGHT_RADIUS)).flatten() ) ) )
	for i, lb in enumerate(rescale(vertices)):
		lb_list.append( pyglet.text.Label(str(i), font_name=FONT, font_size=160, x=lb[0], y=lb[1], anchor_x='center', anchor_y='center', color = (0, 0, 0, 255)) )

	status = pyglet.text.Label('Current turn: Red. Selected piece:', font_name=FONT, font_size = 12, x = 10, y = window.height - 10, anchor_x = 'left', anchor_y = 'top', color = (0, 0, 0, 255) )
	warning = pyglet.text.Label('', font_name=FONT, font_size = 12, x = - 10 + window.width, y = 10, anchor_x = 'right', anchor_y = 'bottom', color = (255, 0, 0, 255) )

	def update_labels():
		if ACTIVE_PLAYERS[currentturn % PLAYERS]:
			status.text = f'Turn {currentturn//PLAYERS + 1} ({playernames[owner(currentturn)]} to move). Selected piece: {("None" if selected == -1 else pieces[occupied[selected] // PLAYERS][occupied[selected] % PLAYERS])}'
		else:
			status.text = f'Turn {currentturn//PLAYERS + 1} ({playernames[owner(currentturn)]} defeated). Press K to skip turn.'
		for i in range(len(vertices)):
			if occupied[i] == -1: 
				lb_list[i].text = str(i)
				lb_list[i].color = (0 , 0 , 0 , 128)
				lb_list[i].font_size = 30
			else:
				lb_list[i].text = pieces[occupied[i] // PLAYERS][occupied[i] % PLAYERS]
				lb_list[i].color = textcolours[occupied[i] % PLAYERS]
				lb_list[i].font_size = 160

	update_labels()

	@window.event
	def on_draw():
		GL.glClearColor(*WHITE)
		window.clear()

		global viewscale
		global viewport

		GL.glMatrixMode(GL.GL_MODELVIEW)

		a = (GL.GLfloat * 16)()
		mvm = GL.glGetFloatv(GL.GL_MODELVIEW_MATRIX, a)
		viewport = np.array (list(a)[-4:-2])

		oldscale = viewscale
		viewscale = (scale * viewscale**ZOOM_SMOOTH)**(1/(ZOOM_SMOOTH + 1))
		GL.glScalef(viewscale / oldscale, viewscale / oldscale, viewscale / oldscale)
		GL.glTranslatef(zoompos[0] * (1 - viewscale / oldscale) , zoompos[1] * (1 - viewscale / oldscale),  0)

		GL.glColor4f(*GRAY)

		for i, eq in enumerate(equipotentials):
			eq_list[i].draw(GL.GL_LINE_LOOP)

		for i, fl in enumerate(fieldlines):
			fl_list[i].draw(GL.GL_LINE_STRIP)

		GL.glColor4f(*BLACK)

		for i, hl in enumerate(e_doubled):
			ov_list[i].draw(GL.GL_LINE_LOOP)


		for i, vx in enumerate(vertices):
			if threatened[i] != 0 or showgrid:
				if occupied[i] == -1:
					GL.glColor4f(*GREEN)
					vx_list[i].draw(GL.GL_POLYGON)
					GL.glColor4f(*BLACK)
					vx_list[i].draw(GL.GL_LINE_LOOP)
					lb_list[i].draw()
				elif not showgrid:
					GL.glColor4f(*RED)
					hl_list[i].draw(GL.GL_POLYGON)
					GL.glColor4f(*BLACK)
					hl_list[i].draw(GL.GL_LINE_LOOP)

			if selected == i:
				GL.glColor4f(*GREEN)
				hl_list[i].draw(GL.GL_POLYGON)
				GL.glColor4f(*BLACK)
				hl_list[i].draw(GL.GL_LINE_LOOP)

		# We draw the pieces separately from the other GUI elements for z-ordering purposes

		for i, vx in enumerate(vertices):
			if occupied[i] != -1: 
				GL.glColor4f(*PIECE_COLOUR)
				pc_list[i].draw(GL.GL_POLYGON)
				GL.glColor4f(*BLACK)
				pc_list[i].draw(GL.GL_LINE_LOOP)
				lb_list[i].draw()


		GL.glPushMatrix()
		GL.glLoadIdentity()

		# Overlay display goes here
		status.draw()
		warning.draw()
		fps_display.draw()
		GL.glPopMatrix()

	@window.event
	def on_mouse_drag(x, y, dx, dy, button, modifiers):
		if button == pyglet.window.mouse.LEFT: return
		global viewport;
		global viewscale
		GL.glMatrixMode(GL.GL_MODELVIEW)
		GL.glTranslatef(dx / viewscale, dy / viewscale, 0)

	@window.event
	def on_mouse_scroll(x, y, dx, dy):
		global zoompos;
		global scale;

		zoompos = (np.array([x,y])  - viewport) / (viewscale)
		scale = (scale / 1.1 if dy < 0 else scale * 1.1)

	@window.event
	def on_resize(width, height):
		status.y = height - 10
		warning.x = width - 10

	##########################
	# MODEL HANDLING STUFF
	##########################

	showgrid = False

	@window.event
	def on_mouse_release(x, y, button, modifiers):
		if button != pyglet.window.mouse.LEFT: return
		global selected
		global currentturn
		global occupied
		global threatened

		clicked = -1
		mousecoords = np.array([x, y] - viewport) / viewscale / DISPLAY_SCALE
		dist = 100000
		for i, vx in enumerate(vertices):
			if np.linalg.norm(mousecoords - vx) <= PIECE_RADIUS and np.linalg.norm(mousecoords - vx) < dist:
				clicked = i
				dist = np.linalg.norm(mousecoords - vx)
		if clicked == -1 or clicked == selected:
			selected = -1
			warning.text = ''
		else:
			if selected == -1 and owner(occupied[clicked]) == owner(currentturn) and occupied[clicked] != -1:
				selected = clicked
			elif threatened[clicked] and (occupied[clicked] == -1 or owner(occupied[clicked]) != owner(currentturn)):
				newstate = np.array(occupied)
				newstate[clicked] = newstate[selected]
				newstate[selected] = -1
				# Test if move puts self in check, or does not remove self from check
				incheck = (owner(currentturn) in check(newstate))
				if incheck:
					warning.text = 'Current player (%s) will end turn in check.' % playernames[owner(currentturn)]
				else:
					histlist.append("%s: %s (%d)-%d" % (playernames[owner(currentturn)], pieces[occupied[selected] // PLAYERS][owner(occupied[selected])], selected, clicked))
					history.append(occupied)
					# check if move defeats any players
					if -1 < occupied[clicked] < PLAYERS:
						winner = currentturn % PLAYERS
						loser = occupied[clicked]
						defeat(newstate, winner, loser)
						ACTIVE_PLAYERS[loser] = 0
					currentturn += 1
					occupied = np.array(newstate)
					selected = -1
					warning.text = ''
		threatened = threatlist(selected, occupied)
		update_labels()

	@window.event
	def on_key_press(symbol, modifiers):
		global showgrid
		if symbol == pyglet.window.key.S:
			showgrid = True

	@window.event
	def on_key_release(symbol, modifiers):
		global occupied
		global currentturn
		global threatened
		global selected
		global showgrid
		if symbol == pyglet.window.key.R:
			reset()
			warning.text = ''
		if symbol == pyglet.window.key.K:
			histlist.append(f"{playernames[owner(currentturn)]}: SKIPPED")
			history.append(occupied)
			currentturn += 1
			selected = -1
			warning.text = ''
			occupied = np.array(occupied)
		if symbol == pyglet.window.key._1:
			histlist.append(f"{playernames[owner(currentturn)]}: RESIGNED to {playernames[owner(currentturn-1)]}")
			history.append(occupied)
			winner = (currentturn - 1) % PLAYERS
			loser = (currentturn) % PLAYERS
			newstate = np.copy(occupied)
			defeat(newstate, winner, loser)
			ACTIVE_PLAYERS[loser] = 0
			occupied = np.array(newstate)
			currentturn += 1
			selected = -1
			warning.text = ''
		if symbol == pyglet.window.key._2:
			histlist.append(f"{playernames[owner(currentturn)]}: RESIGNED to {playernames[owner(currentturn-2)]}")
			history.append(occupied)
			winner = (currentturn - 2) % PLAYERS
			loser = (currentturn) % PLAYERS
			newstate = np.copy(occupied)
			defeat(newstate, winner, loser)
			ACTIVE_PLAYERS[loser] = 0
			occupied = np.array(newstate)
			currentturn += 1
			selected = -1
			warning.text = ''
		elif symbol == pyglet.window.key.U and len(history):
			histlist.pop()
			occupied = history.pop()
			currentturn -= 1
			selected = -1
			warning.text = ''
		elif symbol == pyglet.window.key.L and len(histlist):
			print_log()
		elif symbol == pyglet.window.key.S:
			showgrid = False
		threatened = threatlist(selected, occupied)
		update_labels()

	def game_log(histlist):
		log = ""
		for i in range((len(histlist) + 1) // PLAYERS):
			log += "%d. " % (i + 1,)
			log += histlist[PLAYERS * i]
			try:
				for j in range(1, PLAYERS):
					log += ", " + histlist[PLAYERS * i + j]
			except IndexError:
				pass
			log += "\n"
		return log

	def print_log():
		global histlist
		if(len(histlist)):
			print("=== GAME LOG ===")
		print(game_log(histlist))

	clock.schedule(lambda x, y: 1, 1/40.)

	# window.push_handlers(pyglet.window.event.WindowEventLogger())
	window.maximize() # this seems to make vsync freeze on resize go away??
	pyglet.app.run()
