import numpy as np
from .config import PLAYERS, 士_FLAG, 兵_FLAG 
from .board import vertices, coords, vert, horiz, U0
# ======= CHINESE CHESS =================
# 0 for red, 1 for black, 2 for (optional) green
# Localisation for original game pieces:

# Now we neeed to determine if squares are threatened or not

def owner(pieceid):
	if pieceid == -1:
		return -1
	return pieceid % PLAYERS

def defeat(state, winner, loser):
	state[state == loser] = -1
	m = (state > -1) & (state % PLAYERS == loser)
	state[m] -= loser
	state[m] += winner

def 帥(vertex, state): # One step in any direction; may not exceed the 1st equipotential
	t = []
	c = coords[vertex]

	hlist = [horiz[i] for i in c[0] if i >= 0] + [_ for i in c[1] for _ in vert[i]]

	for h in hlist:
		p = h.index(vertex)
		for i in [-1, 1]:
			test = (p + i) % len(h)
			if ((h[test] not in t) and (state[h[test]] == -1 or (state[h[test]] != -1 and  owner(state[h[test]]) != owner(state[vertex]))) and coords[h[test]][0][0] < PLAYERS):
				t.append(h[test])

	# 飞将 threat: will not show up when selecting move vertices, but will affect check checking.
	for h in hlist:
		p = h.index(vertex)
		for i in [-1, 1]:
			for j in range(1, len(h)+1):
				if not (0 <= p + i * j < len(h)): break
				if state[h[p + i * j]] != -1:
					if state[h[p + i * j]] // PLAYERS == 0 and owner(state[vertex]) != owner(state[h[p + i * j]]):
						t.append(h[p + i * j])
					break
	return t

def 仕(vertex, state): # May move freely along the 1st equipotential
	t = []
	c = coords[vertex]

	hlist = [horiz[i] for i in c[0] if i >= 0]

	for h in hlist:
		p = h.index(vertex)
		for k in [-1, 1]:
			for i in range(1, len(h)+1):
				test = (p + i * k) % len(h)
				if state[h[test]] != -1:
					if owner(state[h[test]]) != owner(state[vertex]):
						t.append(h[test])
					break
				if h[test] not in t: t.append(h[test])

	if 士_FLAG:
		vlist = [_ for i in c[1] for _ in vert[i]]
		for v in vlist:
			p = v.index(vertex)
			for i in [-1, 1]:
				test = p + i
				if not 0 <= test < len(v): continue
				if (state[v[test]] == -1 or owner(state[v[test]]) != owner(state[vertex])) and (coords[v[test]][0][0] < 2) and v[test] not in t:
					t.append(v[test])
	return t

def 相(vertex, state): # 2 steps diagonally (one up then one left or vice versa, twice), but blocked diagonally
	t = []
	c = coords[vertex]

	vlist = [_ for i in c[1] for _ in vert[i]]

	for i in [-1, 1]:
		for j in [-1, 1]:
			for v in vlist:
				p1 = v.index(vertex) + i
				if not (0 <= p1 < len(v)): continue
				c1 = coords[v[p1]]
				hlist = [horiz[k] for k in c1[0] if k >= 0]
				for h in hlist:
					p2 = (h.index(v[p1]) + j) % len(h)
					c2 = coords[h[p2]]
					if state[h[p2]] == -1:
						vprimelist = [_ for k in c2[1] for _ in vert[k]]
						for vprime in vprimelist:
							p3 = vprime.index(h[p2]) + i
							if not (0  <=  p3 < len(vprime)): continue
							c3 = coords[vprime[p3]]
							hprimelist = [horiz[k] for k in c3[0] if k >= 0]
							for hprime in hprimelist:
								p4 = (hprime.index(vprime[p3]) + j) % len(hprime)
								c4 = coords[hprime[p4]]
								if state[hprime[p4]] == -1 or owner(state[hprime[p4]]) != owner(state[vertex]):
									if len(c4[0]) and c4[0][0] > U0: continue
									if hprime[p4] not in t: t.append(hprime[p4])

	hlist = [horiz[i] for i in c[0] if i >= 0]
	for i in [-1, 1]:
		for j in [-1, 1]:
			for h in hlist:
				p1 = (h.index(vertex) + i) % len(h)
				c1 = coords[h[p1]]
				vlist = [_ for k in c1[1] for _ in vert[k]]
				for v in vlist:
					p2 = (v.index(h[p1]) + j)
					if not (0 <= p2 < len(v)): continue
					c2 = coords[v[p2]]
					if state[v[p2]] == -1:
						hprimelist = [horiz[k] for k in c2[0] if k >= 0]
						for hprime in hprimelist:
							p3 = (hprime.index(v[p2]) + i) % len(hprime)
							c3 = coords[hprime[p3]]
							vprimelist = [_ for k in c3[1] for _ in vert[k]]
							for vprime in vprimelist:
								p4 = (vprime.index(hprime[p3]) + j)
								if not (0  <=  p4 < len(vprime)): continue
								c4 = coords[vprime[p4]]
								if state[vprime[p4]] == -1 or owner(state[vprime[p4]]) != owner(state[vertex]):
									if len(c4[0]) and c4[0][0] > U0: continue
									if vprime[p4] not in t: t.append(vprime[p4])
	return t

def 傌(vertex, state): # One straight and one diagonal; can be blocked on straight step
	t = []
	c = coords[vertex]

	hlist = [horiz[i] for i in c[0] if i >= 0]
	vlist = [_ for i in c[1] for _ in vert[i]]

	for h in hlist:
		p = h.index(vertex)
		for i in [1, -1]:
			if state[ h[(p + i) % len(h)] ] == -1:
				cprime = coords[ h[(p + 2 * i) % len(h)] ]
				vprimelist = [_ for j in cprime[1] for _ in vert[j]]
				for v in vprimelist:
					pprime = v.index( h[(p + 2 * i) % len(h)] )
					for j in [-1, 1]:
						test = pprime + j
						if not (0 <= test < len(v)): continue
						if state[v[test]] == -1 or owner(state[v[test]]) != owner(state[vertex]):
							if(v[test] not in t): t.append(v[test])

	for v in vlist:
		p = v.index(vertex)
		for i in [1, -1]:
			if not (0 <= p + 2 * i < len(v)): continue
			if state[v[p + i]] != -1: continue
			cprime = coords[ v[p + 2 * i] ]
			hprimelist = [horiz[j] for j in cprime[0] if j >= 0]
			for h in hprimelist:
				pprime = h.index( v[p + 2 * i] )
				for j in [-1, 1]:
					test = (pprime + j) % len(h)
					if state[h[test]] == -1 or owner(state[h[test]]) != owner(state[vertex]):
						if h[test] not in t: t.append(h[test])
	return t

def 火(vertex, state):
	t = []
	c = coords[vertex]

	hlist = [horiz[i] for i in c[0] if i >= 0]
	vlist = [_ for i in c[1] for _ in vert[i]]

	for h in hlist:
		p = h.index(vertex)
		for i in [1, -1]:
			if state[ h[(p + i) % len(h)] ] == -1 and state[ h[(p + 2*i) % len(h)] ] == -1:
				cprime = coords[ h[(p + 3 * i) % len(h)] ]
				vprimelist = [_ for j in cprime[1] for _ in vert[j]]
				for v in vprimelist:
					pprime = v.index( h[(p + 3 * i) % len(h)] )
					for j in [-1, 1]:
						test = pprime + j
						if not (0 <= test < len(v)): continue
						if state[v[test]] == -1 or owner(state[v[test]]) != owner(state[vertex]):
							if(v[test] not in t): t.append(v[test])

	for v in vlist:
		p = v.index(vertex)
		for i in [1, -1]:
			if not (0 <= p + 3 * i < len(v)): continue
			if state[v[p + i]] != -1 or state[v[p + 2*i]] != -1: continue
			cprime = coords[ v[p + 3 * i] ]
			hprimelist = [horiz[j] for j in cprime[0] if j >= 0]
			for h in hprimelist:
				pprime = h.index( v[p + 3 * i] )
				for j in [-1, 1]:
					test = (pprime + j) % len(h)
					if state[h[test]] == -1 or owner(state[h[test]]) != owner(state[vertex]):
						if h[test] not in t: t.append(h[test])
	return t


def 俥(vertex, state): # Move and capture freely along any line
	t = []
	c = coords[vertex]

	hlist = [horiz[i] for i in c[0] if i >= 0]
	vlist = [_ for i in c[1] for _ in vert[i]]

	for h in hlist:
		p = h.index(vertex)
		for k in [-1, 1]:
			for i in range(1, len(h)+1):
				test = (p + i * k) % len(h)
				if state[h[test]] != -1:
					if owner(state[h[test]]) != owner(state[vertex]):
						t.append(h[test])
					break
				if h[test] in t: continue
				t.append(h[test])

	for v in vlist:
		p = v.index(vertex)
		for k in [-1, 1]:
			for i in range(1, len(v) + 1):
				test = p + i * k
				if not (0 <= test < len(v)): break
				if state[v[test]] != -1:
					if owner(state[v[test]]) != owner(state[vertex]):
						t.append(v[test])
					break
				if v[test] in t: continue
				t.append(v[test])

	return t

def 炮(vertex, state): # Move freely; can only capture by skipping one blocked vertex
	t = []
	c = coords[vertex]

	hlist = [horiz[i] for i in c[0] if i >= 0]
	vlist = [_ for i in c[1] for _ in vert[i]]

	for h in hlist:
		p = h.index(vertex)
		for k in [-1, 1]:
			blocked = False
			for i in range(1, len(h)+1):
				test = (p + i * k) % len(h)
				if state[h[test]] != -1:
					if blocked:
						if owner(state[h[test]]) != owner(state[vertex]):
							t.append(h[test])
						break
					else:
						blocked = True
						continue
				if h[test] in t: continue
				if not blocked: t.append(h[test])

	for v in vlist:
		p = v.index(vertex)
		for k in [-1, 1]:
			blocked = False
			for i in range(1, len(v)+ 1):
				test = p + i * k
				if not (0 <= test < len(v)): break
				if state[v[test]] != -1:
					if blocked:
						if owner(state[v[test]]) != owner(state[vertex]):
							t.append(v[test])
						break
					else:
						blocked = True
						continue
				if v[test] in t: continue
				if not blocked: t.append(v[test])

	return t

def 兵(vertex, state): # Advance out of own well; move freely on first connected equipotential; descend or move one step sideways into enemy well.
	t = []
	c = coords[vertex]

	hlist = [horiz[i] for i in c[0] if i >= 0]
	vlist = [_ for i in c[1] for _ in vert[i]]

	if len(c[0]) and (0 <= c[0][0] < U0 and owner(c[0][0]) == owner(state[vertex])):
		vmove = 1
	else: vmove = -1

	if vmove == -1:
		if len(c[0]) and c[0][0] == U0:
			for h in hlist:
				p = h.index(vertex)
				for k in [-1, 1]:
					for i in range(1, len(h)+1):
						test = (p + k * i) % len(h)
						if state[h[test]] != -1: break
						if h[test] in t: break
						t.append(h[test])
		else:
			for h in hlist:
				p = h.index(vertex)
				for i in [-1, 1]:
					test = (p + i) % len(h)
					if (h[test] not in t) and (state[h[test]] == -1 or (state[h[test]] != -1 and  owner(state[h[test]]) != owner(state[vertex]))):
						t.append(h[test])

	for v in vlist:
		p = v.index(vertex)
		test = p + vmove
		if not (0 <= test < len(v)):
			continue
		if owner(state[vertex]) == owner(state[v[test]]):
			continue
		if vmove == -1:
			if len(coords[v[test]][0]) == 0:
				continue
			if state[v[test]] == -1 and (coords[v[test]][0][0] >= 0 and owner(coords[v[test]][0][0]) == owner(state[vertex])):
				continue
			if coords[v[test]][0][0] >= U0:
				continue
		if not 兵_FLAG and coords[v[test]][0][0] < 0:
			continue
		t.append(v[test])

	return t

dispatch = [帥, 仕, 相, 傌, 俥, 炮, 兵, 火]

def threatlist(selected, state, init = None):
	global dispatch
	global vertices
	threat = np.array([0 for i in vertices], dtype=int) + (init if init is not None else 0)
	if selected != -1:
		t = dispatch[state[selected] // PLAYERS](selected, state)
		for i in t:
			threat[i] = 1
	return threat

def check(state):
	global vertices
	threat = np.array([0 for i in vertices], dtype=int)
	for i, val in enumerate(state):
		if val != -1:
			threat += threatlist(i, state)
	threat = np.sign(threat)
	c  = []
	for i, t in enumerate(threat):
		if threat[i] == 1 and state[i] != -1 and state[i] // PLAYERS == 0: c.append (owner(state[i]))
	return c