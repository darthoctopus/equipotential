import numpy as np
from scipy.integrate import odeint
from scipy.optimize import fsolve, minimize
from scipy.interpolate import interp1d

EPSILON = 0.001

def generate_board(U, Ux, Uy, U0=0, MAX_U=8, etime=None, esteps=None,
	N_enclosed=5, starting_positions=None, rU=None, dU=None, Umin=None, y0=None):

	'''
	MAX_U: controls number of equipotentials shown
	'''

	if starting_positions is None:
		starting_positions = [[-1, 0], [1, 0]]

	players = len(starting_positions)

	# assumptions: starting positions exhibit rotational symmetry around origin

	x0 = starting_positions[0][0]
	θ0 = [np.arctan2(*_) for _ in starting_positions]
	Δθ = [_ - θ0[0] for _ in θ0]

	# Plot equipotentials: we need some math first

	def integrate(point, Ux, Uy, tmax=5, dts=100):
		return odeint(lambda w, t, p: [Ux(w[0], w[1]), Uy(w[0], w[1])], point, np.linspace(0,tmax,dts), (0,) , rtol=1e-20)


	# We space equipotentials out evenly at constant ratios of potential near
	# the Coulomb sources (idea by YCX to make things look better). The remainder of
	# the equipotentials are spaced out linearly.

	test_e = []

	equipotentials = []
	fieldlines = []

	Uold = Umin

	for i in range(MAX_U + 1):
		y0 = fsolve(lambda y: U(x0, y) - (Uold * rU if i < N_enclosed else Uold + dU), y0)
		if(i < N_enclosed): 
			dy = y0 - starting_positions[0][1]
			for (x, y), δθ in zip(starting_positions, Δθ):
				test_e.append((float(x + y0 * np.sin(δθ)), float(y + y0 * np.cos(δθ)), etime[i], esteps[i]))
			U0 += players
		else:
			test_e.append((x0, y0, etime[i], esteps[i]))
		y0 = test_e[-1][1]
		Uold = U(test_e[-1][0], test_e[-1][1])

	print('Computing equipotentials:')

	for thing in test_e:
		equipotentials.append(integrate((thing[0], thing[1]), Uy, lambda x, y: -1 * Ux(x, y), thing[2], thing[3]))

	print('Computing fieldlines:')

	test_f = [(x + EPSILON * np.cos(np.pi / 4 * theta - δθ),
		y + EPSILON * np.sin(np.pi / 4 * theta - δθ),
		1, 100) for (x, y), δθ in zip(starting_positions, Δθ) for theta in range(1,8)]
	for thing in test_f:
		fieldlines.append(integrate((thing[0], thing[1]), Ux, Uy, thing[2], thing[3]))

	return U0, equipotentials, fieldlines

def connect_graph(equipotentials, fieldlines, players, vertices=None, coords=None, n_fiat=0):
	# Graph connectivity: define some vertices by fiat
	# (saddle points, source/sinks, axes)

	def intersect(p1, p2, q1, q2):
		x1, y1 = p1
		x2, y2 = p2
		x3, y3 = q1
		x4, y4 = q2
		uu = np.dot(np.linalg.inv(np.array([[x2 - x1, x3 - x4], [y2 - y1, y3 - y4]])), np.array([x3 - x1, y3 - y1]))
		if uu[0] > 0 and uu[0] <= 1 and uu[1] > 0 and uu[1] <= 1:
			return q1 + (q2 - q1) * uu[1]
		else:
			return None

	## This was my old point-in-polygon query routine.
	## Unfortunately this thing only works for convex polygons

	# def inside(point, polygon):
	#     vectors = [np.dot(np.array([[0, -1],[1, 0]]), polygon[i] - polygon[i-1]) for i, p in enumerate(polygon)]
	#     anchors = [point - p for p in polygon]
	#     dots = [np.dot(p, q) for p, q in zip(anchors, vectors)]
	#     total = np.sum(np.sign(dots))
	#     return True if total / len(vectors) == -1 else False

	def inside(point, polygon):
		odd = False
		for i, p in enumerate(polygon):
			if (((p[1] < point[1] and polygon[i-1][1] >= point[1]) or (polygon[i-1][1] < point[1] and p[1] >= point[1])) and (p[0] <= point[0] or polygon[i-1][0] <= point[0])):
				if (p[0] + (point[1] - p[1])/(polygon[i-1][1] - p[1]) * (polygon[i-1][0] - p[0]) < point[0]):
					odd = not odd
		return odd

	print('Computing intersections:')
	if vertices is None:
		vertices = []
	if coords is None:
		coords = []

	# home positions
	for j in range(players):
		coords.append([[-1], [i for i, fl in enumerate(fieldlines) if inside(fl[0], equipotentials[j])]])

	# Find intersections for fiat field lines
	# We are using a PAINFULLY INEFFICIENT algorithm. I weep to read what I have written.
	# Unfortunately I can't think of a better way to do this
	# (This seems to be a recurring problem…)

	for eindex, eq in enumerate(equipotentials):
		for findex, fl in enumerate(fieldlines[0:n_fiat]):
			for i in range(len(eq) - 1):
				for j in range(len(fl)):
						z = intersect(eq[i], eq[i+1], fl[j], fl[j-1])
						if z is not None:
							if not len(vertices) or np.linalg.norm(z - vertices[-1]) > 0.01:
								vertices.append(z)
								coords.append([[eindex], [findex]])
							# print(U(z[0], z[1]))
					# break

	# Find intersections with other field line
	# Insight: each field line crosses each level set exactly once,
	# although not necessarily each connected component of such a set.
	# We don't need exact intersections!

	print('Computing more intersections:')

	for i, fl in enumerate(fieldlines[n_fiat:]):
		for j, eq in enumerate(equipotentials):
			# We do binary search now
			start = 0
			end = len(fl) - 1
			if inside(fl[start], eq) == inside(fl[end], eq): continue
			while end - start > 1:
				pivot = (start + end) // 2
				check = inside(fl[pivot], eq)
				if check: start = pivot
				else: end = pivot
			for k in range(len(eq)):
				z = intersect(fl[start], fl[end], eq[k], eq[k-1])
				if z is not None:
					vertices.append(z)
					coords.append([[j], [i+n_fiat]])

	return vertices, coords

def grid(U0, equipotentials, fieldlines, coords, players=2):
	# Except for the saddle points and sources, each vertex 
	# is assigned to exactly one equipotential and one
	# field line; we define connectivity by means of two different
	# valency lists. We also define two senses of movement 
	# (for indexing purposes) as counterclockwise (along
	# equipotentials) and outwards (along field lines).

	# Therefore, ordinary vertices are determined to be adjacent
	# if either their equipotential or their field line list-index differ
	# by exactly 1 (which list exactly depends on where they are).

	# def horiz(v_index): #counterclockwise by default
	#     global vertices
	#     global coords
	#     if v_index in [0,1,2]: return None
	#     if U(vertices[v_index][0], vertices[v_index][1]) < U0:
	#         alist = coords[1 if vertices[v_index][0] < 0 else 2][1]
	#     else: 
	#         alist = [1] + coords[2][1][1:] + [0] + [i for i in reversed(coords[1][1][1:])]
	#     c = coords[v_index]
	#     e = c[0][0]
	#     c = [i  for ff in alist for i, v in enumerate(coords) if e in v[0] and ff in v[1]]
	#     return c

	# def vert(v_index):
	#     global coords
	#     hitlist = []
	#     for fl in coords[v_index][1]:
	#         vlist = [i for i, v in enumerate(coords) if fl in v[1]] #automagically ordered by potential value
	#         if fl in [2,3]: vlist = vlist[1:] + [0]
	#         hitlist.append(vlist)
	#     return hitlist

	horiz = []

	'''
	Equipotential adjacency: each equipotential has one element of this list,
	which is a list of vertex IDs ordered by adjacency for that equipotential.
	'''

	for e in range(len(equipotentials)):
		# get IDs of valid field lines associated with that equipotential
		if e < U0:
			alist = coords[1 + e % players][1]
		else: 
			# some contortion needed to maintain correct ordering of field lines counterclockwise
			alist = []
			for i in range(players):
				q = players - i
				alist.append(q-1)
				alist.extend(coords[q][1][1:])
		horiz.append ( [i  for ff in alist for i, v in enumerate(coords) if e in v[0] and ff in v[1]] )

	vert = []

	'''
	Field line adjacency: each field line has one element of this list,
	which is a list of further lists of vertex IDs ordered *outwards* for
	all possible field-line-based trajectories.
	'''

	for f in range(len(fieldlines)):
		vert.append([[i for i, v in enumerate(coords) if f in v[1]]])

	return horiz, vert