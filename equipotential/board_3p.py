import numpy as np
from .math import generate_board, connect_graph, grid
from .config import PLAYERS, EXTRA_兵, OPT_PIECES

# 3 player chess (based on 三国旗)
# Starting poisitions will be points on equilateral triangle.

ALPHA = 1
BETA = 0.125

q = np.sqrt(3)/3
r  = np.sqrt(3) - q

starting_positions = [[-r, 0], [q, 1], [q, -1]]

def U1(x, y, x0, y0):
	return 1 / np.sqrt((x - x0)**2 + (y - y0)**2)

def U1x(x, y, x0, y0):
	return (x - x0) / np.sqrt((x - x0)**2 + (y - y0)**2)**3

def U1y(x, y, x0, y0):
	return (y - y0) / np.sqrt((x - x0)**2 + (y - y0)**2)**3

def U(x, y):
	return - 1 * ALPHA * (U1(x, y, q, 1) + U1(x, y, q, -1) + U1(x, y, -r, 0)) + BETA * (x**2 + y**2)

def Ux(x, y):
	return ALPHA * (U1x(x, y, q, 1) + U1x(x, y, q, -1) + U1x(x, y, -r, 0)) + BETA * x

def Uy(x, y):
	return ALPHA * (U1y(x, y, q, 1) + U1y(x, y, q, -1) + U1y(x, y, -r, 0)) + BETA * y

# Unfortunately, I can't think of a better way of plotting equipotentials for now. 
# These magic numbers are chosen to minimise graphical artifacts when ODE-solving

etime = [.135, .22, .36, .64, 1.3, 5.2, 5, 6, 8, 16.30, 21.2, 26, 30, 33.5, 36.4]
esteps = [50, 50, 50, 50, 50, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100]

# Equipotentials are spaced out logarithmically near the wells and linearly outside the wells

rU = U(-r, 0.65) / U(-r, 0.55)
dU = U(-r, 0.65) - U(-r, 0.55)

# "zeroth" equipotential to draw: this will not actually be drawn

y0 = 0.24
Umin = U(-r, y0)

U0, equipotentials, fieldlines = generate_board(U, Ux, Uy, etime=etime, esteps=esteps,
												rU=rU, dU=dU, Umin=Umin, y0=y0,
												starting_positions=starting_positions)

# Nonphysical streamlines and equipotentials: we need to add the saddle point
# field lines directly

fiat = [np.array([[0, 0], [-2*q, 2]]),
		np.array([[0, 0], [2 * r, 0]]),
		np.array([[0, 0], [-2*q, -2]]),
		np.array([[q, -1], [0, 0]]),
		np.array([[-r, 0], [0, 0]]),
		np.array([[q, 1], [0, 0]])
		]
n_fiat = len(fiat)
fieldlines = [*fiat, *fieldlines]

# # We want some equipotentials to show up as double lines when rendered.

# e_doubled = [0, 1, 10]
# e_displ = [np.array([j, 0]) for j in [-1, 1, 0]]
# e_offsets = [40, 40, 20]

e_doubled = [0, 1, 2, 15]
e_displ = [*starting_positions, [0,0]]
e_offsets = [60 ,60, 60, 30]

# fiat vertices: home positions and saddle points
vertices = [np.array([0,0]), np.array([-r, 0]), np.array([q, 1]), np.array([q, -1])]

# saddle point intersections
coords = [[[], [0, 1, 2, 3, 4, 5]]]

vertices, coords = connect_graph(equipotentials, fieldlines,
	players=PLAYERS, vertices=vertices, coords=coords, n_fiat=len(fiat))
horiz, vert = grid(U0, equipotentials, fieldlines, coords, players=PLAYERS)

# Deal with saddle points by fiat
# Fix zero points for rays intersecting the point masses
for i in range(PLAYERS):
	vert[i + PLAYERS][0] = vert[i + PLAYERS][0][1:] + [vert[i + PLAYERS][0][0]]

# Now, geometrically some of the field lines are allowed to be continuous
# (at the point sources and saddle points); we will intervene manually.

# glue up things: sources

pairs = [(coords[i+1][1][j], coords[i+1][1][j+4]) for i in range(PLAYERS) for j in range(len(coords[i+1][1]) // 2)]

def glue(a, b, ia=0, ib=0):
	global vert
	c = [i for i in reversed(vert[b][ib][1:])]
	vert[a][ia] = c + vert[a][ia]
	vert[b][ib] = [i for i in reversed(vert[a][ia])]

for pair in pairs:
	glue(*pair)

# Directly connect the point masses through the saddle point

saddles = [vert[i+3][0].copy() for i in range(PLAYERS)] # from point masses to saddles
centers = [vert[i+3][0] + vert[(i+1)%3+3][0][:-1][::-1] for i in range(PLAYERS)] # from point masses to other point masses

for i in range(PLAYERS):
	vert[3+i] = [centers[i], centers[i-1][::-1]]
	vert[n_fiat + 3 + (i-1)%3*7] = [centers[i][::-1], centers[i-1]]

for i in range(PLAYERS):
	l = saddles[i][:-1] + vert[i][0]
	vert[i][0] = l.copy()
	vert[i+3].append(l.copy())
	vert[n_fiat + 3 + (i-1)%3*7].append(l[::-1])

# Let us do the actual laying out!

def reset_board():
	occupied = np.array([-1 for i in vertices], dtype=int)
	print('Setting up board:')

	occupied[1] = PLAYERS * 0 + 0
	occupied[40] = PLAYERS * 1 + 0
	occupied[76] = PLAYERS * 1 + 0
	occupied[41] = PLAYERS * 2 + 0
	occupied[77] = PLAYERS * 2 + 0
	occupied[50] = PLAYERS * 3 + 0
	occupied[68] = PLAYERS * 3 + 0
	occupied[58] = PLAYERS * 4 + 0
	occupied[59] = PLAYERS * 4 + 0
	occupied[52] = PLAYERS * 5 + 0
	occupied[70] = PLAYERS * 5 + 0
	occupied[16] =  PLAYERS * 6 + 0
	occupied[44] = PLAYERS * 6 + 0
	occupied[62] = PLAYERS * 6 + 0
	occupied[80] = PLAYERS * 6 + 0

	occupied[2] = PLAYERS * 0 + 1
	occupied[103] = PLAYERS * 1 + 1
	occupied[139] = PLAYERS * 1 + 1
	occupied[104] = PLAYERS * 2 + 1
	occupied[140] = PLAYERS * 2 + 1
	occupied[113] = PLAYERS * 3 + 1
	occupied[131] = PLAYERS * 3 + 1
	occupied[121] = PLAYERS * 4 + 1
	occupied[122] = PLAYERS * 4 + 1
	occupied[115] = PLAYERS * 5 + 1
	occupied[133] = PLAYERS * 5 + 1
	occupied[17] =  PLAYERS * 6 + 1
	occupied[107] = PLAYERS * 6 + 1
	occupied[125] = PLAYERS * 6 + 1
	occupied[143] = PLAYERS * 6 + 1

	occupied[3] = PLAYERS * 0 + 2
	occupied[166] = PLAYERS * 1 + 2
	occupied[202] = PLAYERS * 1 + 2
	occupied[167] = PLAYERS * 2 + 2
	occupied[203] = PLAYERS * 2 + 2
	occupied[176] = PLAYERS * 3 + 2
	occupied[194] = PLAYERS * 3 + 2
	occupied[184] = PLAYERS * 4 + 2
	occupied[185] = PLAYERS * 4 + 2
	occupied[178] = PLAYERS * 5 + 2
	occupied[196] = PLAYERS * 5 + 2
	occupied[18] =  PLAYERS * 6 + 2
	occupied[170] = PLAYERS * 6 + 2
	occupied[188] = PLAYERS * 6 + 2
	occupied[206] = PLAYERS * 6 + 2

	if OPT_PIECES:
		occupied[34] = PLAYERS * 7 + 0
		occupied[88] = PLAYERS * 7 + 0
		occupied[97] = PLAYERS * 7 + 1
		occupied[151] = PLAYERS * 7 + 1
		occupied[160] = PLAYERS * 7 + 2
		occupied[214] = PLAYERS * 7 + 2

	if EXTRA_兵:
		occupied[13] = PLAYERS * 6 + 0
		occupied[14] = PLAYERS * 6 + 1
		occupied[15] = PLAYERS * 6 + 2

	# NOTE: these were generated manually. Layout might need to be tweaked
	# if board geometry changes significantly (for future work)
	return occupied