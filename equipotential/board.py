from .config import PLAYERS

if PLAYERS == 2:
	from .board_2p import (equipotentials, fieldlines,
		vertices, coords, vert, horiz, U0, reset_board, e_doubled, e_displ, e_offsets)
elif PLAYERS == 3:
	from .board_3p import (equipotentials, fieldlines,
		vertices, coords, vert, horiz, U0, reset_board, e_doubled, e_displ, e_offsets)