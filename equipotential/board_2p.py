import numpy as np
from .math import generate_board, connect_graph, grid
from .config import PLAYERS, EXTRA_兵

## The equipotential coordinate system
# Equipotentials are defined by the constraint U(x, y) = C for some constant C. 
# This yields a series of curves (level sets of U) on the x-y plane.
# For open subsets of the x-y plane in which grad U is nonsingular, this sets up
# a local coordinate system parameterised by U and some other function V 
# satisfying V_x U_x + V_y U_y = 0 on this open set. 
# Note that this constraint is automatically satisfied if U and V are the real
# and imaginary parts of a complex-valued function f(x + iy) = U + iV. However,
# this is not the case for the Coulomb potential. However, we can still use the
# Cauchy-Riemann conditions as a PDE in V on local sets 

# Plot equipotentials: we need some math first

ALPHA = 1
BETA = 0.125

def U1(x, y, x0, y0):
	return 1 / np.sqrt((x - x0)**2 + (y - y0)**2)

def U1x(x, y, x0, y0):
	return (x - x0) / np.sqrt((x - x0)**2 + (y - y0)**2)**3

def U1y(x, y, x0, y0):
	return (y - y0) / np.sqrt((x - x0)**2 + (y - y0)**2)**3

def U(x, y):
	return - 1 * ALPHA * (U1(x, y, 1, 0) + U1(x, y, -1, 0)) + BETA * (x**2 + y**2)

def Ux(x, y):
	return ALPHA * (U1x(x, y, 1, 0) + U1x(x, y, -1, 0)) + BETA * x

def Uy(x, y):
	return ALPHA * (U1y(x, y, 1, 0) + U1y(x, y, -1, 0)) + BETA * y


# Unfortunately, I can't think of a better way of plotting equipotentials for now. 
# These magic numbers are chosen to minimise graphical artifacts when ODE-solving

# etime = [0.229, 0.315, 0.452, 0.685, 1.15, 5.05, 5.15, 6.95, 10., 14, 18.5]
# esteps = [50, 50, 50, 50, 50, 100, 100, 100, 100, 100, 100]

etime = [.16, .27, .46, .81, 1.6, 4.65, 5.90, 8.3, 11.83, 16.30, 21.2, 26, 30, 33.5, 36.4]
esteps = [50, 50, 50, 50, 50, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100]

# Equipotentials are spaced out logarithmically near the wells and linearly outside the wells

rU = U(1, 0.65) / U(1, 0.55)
dU = U(1, 0.65) - U(1, 0.55)

# "zeroth" equipotential to draw: this will not actually be drawn

y0 = 0.25
Umin = U(1, y0)

U0, equipotentials, fieldlines = generate_board(U, Ux, Uy, etime=etime, esteps=esteps,
												rU=rU, dU=dU, Umin=Umin, y0=y0)

# Nonphysical streamlines and equipotentials: we need to add the saddle point
# field lines directly

fiat = [np.array([[0, 0], [0, 3]]), np.array([[0, 0], [0, -3]]), np.array([[-1, 0], [0, 0]]), np.array([[1, 0], [0, 0]])]
fieldlines = [*fiat, *fieldlines]

# We want some equipotentials to show up as double lines when rendered.

e_doubled = [0, 1, 10]
e_displ = [np.array([j, 0]) for j in [-1, 1, 0]]
e_offsets = [40, 40, 20]

# fiat vertices: home positions and saddle points
vertices = [np.array([0,0]), np.array([-1, 0]), np.array([1, 0])]

# saddle point intersections
coords = [[[], [0, 1, 2, 3]]]

vertices, coords = connect_graph(equipotentials, fieldlines,
	players=PLAYERS, vertices=vertices, coords=coords, n_fiat=len(fiat))
horiz, vert = grid(U0, equipotentials, fieldlines, coords)

# Deal with saddle points by fiat
# Fix zero points for rays intersecting the point masses
for i in range(PLAYERS):
	vert[i + PLAYERS][0] = vert[i + PLAYERS][0][1:] + [vert[i + PLAYERS][0][0]]

# Now, geometrically some of the field lines are allowed to be continuous
# (at the point sources and saddle points); we will intervene manually.

# glue up things: sources

pairs = [(coords[i][1][j], coords[i][1][j+4]) for i in [1, 2] for j in range(len(coords[i][1]) // 2)]

def glue(a, b):
	global vert
	c = [i for i in reversed(vert[b][0][1:])]
	vert[a][0] = c + vert[a][0]
	vert[b][0] = [i for i in reversed(vert[a][0])]

for pair in pairs:
	glue(*pair)

# glue up things: saddle points

glue(0,1)
vert[2][0].reverse()
vert[3][0].reverse()
glue(2,3)
vert[2][0].reverse()
vert[3][0].reverse()
vert[7][0] = vert[3][0]
vert[14][0] = vert[2][0]

# Let us do the actual laying out!

def reset_board():
	occupied = np.array([-1 for i in vertices], dtype=int)
	print('Setting up board:')
	occupied[1] = PLAYERS * 0 + 0
	occupied[30] = PLAYERS * 1 + 0
	occupied[66] = PLAYERS * 1 + 0
	occupied[31] = PLAYERS * 2 + 0
	occupied[67] = PLAYERS * 2 + 0
	occupied[40] = PLAYERS * 3 + 0
	occupied[58] = PLAYERS * 3 + 0
	occupied[48] = PLAYERS * 4 + 0
	occupied[49] = PLAYERS * 4 + 0
	occupied[42] = PLAYERS * 5 + 0
	occupied[60] = PLAYERS * 5 + 0
	occupied[11] =  PLAYERS * 6 + 0
	occupied[34] = PLAYERS * 6 + 0
	occupied[52] = PLAYERS * 6 + 0
	occupied[70] = PLAYERS * 6 + 0

	occupied[2] = PLAYERS * 0 + 1
	occupied[93] = PLAYERS * 1 + 1
	occupied[129] = PLAYERS * 1 + 1
	occupied[94] = PLAYERS * 2 + 1
	occupied[130] = PLAYERS * 2 + 1
	occupied[103] = PLAYERS * 3 + 1
	occupied[121] = PLAYERS * 3 + 1
	occupied[111] = PLAYERS * 4 + 1
	occupied[112] = PLAYERS * 4 + 1
	occupied[105] = PLAYERS * 5 + 1
	occupied[123] = PLAYERS * 5 + 1
	occupied[12] =  PLAYERS * 6 + 1
	occupied[97] = PLAYERS * 6 + 1
	occupied[115] = PLAYERS * 6 + 1
	occupied[133] = PLAYERS * 6 + 1

	if EXTRA_兵:
		occupied[9] = PLAYERS * 6 + 0
		occupied[10] = PLAYERS * 6 + 1

	# NOTE: these were generated manually. Layout might need to be tweaked
	# if board geometry changes significantly (for future work)
	return occupied