from .config import PLAYERS

origpieces = [[u"帥", u"將", 1], 
			  [u"仕", u"士", u"士", 2],
			  [u"相", u"象", u"向", 2],
			  [u"傌", u"馬", u"馬", 2],
			  [u"俥", u"車", u"車", 2],
			  [u"炮", u"砲", u"礮", 2],
			  [u"兵", u"卒", u"勇", 4]]
 
# Localisation for three-player variants:

optpieces = [[u"蜀", u"魏", u"吳", 1], [u"火", u"旗", u"風", 2]]
optnames = [u"蜀", u"魏", u"吳"]

if PLAYERS == 2:
	pieces = origpieces
	playernames = ["Red", "Black"]
	textcolours = [(200, 0, 0, 255), (0, 0, 0, 255)]
elif PLAYERS == 3:
	pieces = [optpieces[0], *origpieces[1:], optpieces[1]]
	textcolours = [(200, 0, 0, 255), (0, 0, 200, 255) , (36, 150, 59, 255)]
	playernames = optnames