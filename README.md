# Equipotential Chess

Chinese chess, except played on equipotential and field lines (and resulting topological weirdness). For more details, see http://hyad.es/equipotential2. 3-player variant explained in some detail at http://hyad.es/equipotential3.

## Requirements

Requires Pyglet (tested on 1.2.1 and 1.2.4) on Python 3 (I used Chinese characters for some of the function names, so this probably won't work as is on Python 2 -- also, on Python 2 one should use `__future__` division. Unexpected side effect of unicode function names: breaks when compiling with Nuitka.)

## Controls

Left-click to select and deselect pieces and positions. Drag with the right or middle mouse buttons to move the map. Scroll to zoom. 

Keyboard controls: 

- U to undo
- R to reset
- L to print the game log to the console
- K to skip turn
- 1 and 2 to resign to previous or next previous player (for 3-player variant)
- ESC to exit
- Hold S to see vertex numbering.

## Known Issues (technical)

- Font rendering kind of sucks (dunno how I should fix this really)
- ~~On some systems (tested with Windows 10 on KLKM's  and KZW's computers), screen flickers when `glPushMatrix()` and `glPopMatrix()` called (no idea why).~~
- Holding S to see vertex numbering kills frame rate

## Known Issues (game balance)

- 中炮 OP pls fix (accordig to KZW) — possibly mitigated by an extra pawn (set `EXTRA_兵 = True`)
- Default board layout might need some tweaking

## Rule changes

- In this version of the game, 士 pieces by default travel between the first equipotentials and their point sources. This can be changed by setting `士_FLAG = False`.
- 兵 pieces may also move up to the first equipotential from enemy point sources; this prevents them from having no valid moves once they reach the point source. This may be disabled by setting `兵_FLAG = False`. In practice this is only a concern for the 2-player game.
- In the 3-player variant, the optional bannerman pieces can be placed by setting `OPT_PIECES = True`

## Screenshots

![Screenshot1](images/screenshot3.png)